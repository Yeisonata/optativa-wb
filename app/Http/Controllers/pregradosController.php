<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pregrados;

class pregradosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pregrados = Pregrados::all();
        return view('crudp.index', compact('pregrados'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $pregrado = new Pregrados();

        if ($request->hasFile('files')) {
            $file = $request->file('files');
            $desimg = 'img/';
            $filename = time() . '_' . $file->getClientOriginalName();
            $uploadSucess = $request->file('files')->move($desimg, $filename);
            $url = url($desimg . $filename);
            $pregrado->img = $url;
        }

        $pregrado->fill($request->except('activo'))->saveOrFail();
        $pregrado->estado = $request->input('estado', false);
        $pregrado->save();

        return redirect('pregrados');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $pregrados = Pregrados::find($id);
        return view('crudp.edit', compact('pregrados'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $pregrado = Pregrados::find($id);
        if ($request->hasFile('files')) {
            $file = $request->file('files');
            $desimg = 'img/';
            $filename = time() . '_' . $file->getClientOriginalName();
            $uploadSucess = $request->file('files')->move($desimg, $filename);
            $url = url($desimg . $filename);
            $pregrado->img = $url;
        }

        $pregrado->fill($request->except('activo'))->saveOrFail();
        $pregrado->estado = $request->input('estado', false);
        $pregrado->save();

        return redirect('pregrados');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $pregrado = Pregrados::find($id);
        $pregrado->delete();
        return redirect('pregrados');
    }
}
