<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Estudiantes;
use  App\Models\Pregrados;

class estudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $estudiantes = Estudiantes::all();
        $pregrados = Pregrados::all();

        return view('crude.index', compact('estudiantes','pregrados'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $estudiantes = new Estudiantes($request->input());
        $estudiantes->saveOrFail();
        return redirect(('estudiantes'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
