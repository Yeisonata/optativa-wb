<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pregrados;

class pregradosController extends Controller
{
    public function getPregrados(){
         return response()->json(Pregrados::all(),200);
    }
}
