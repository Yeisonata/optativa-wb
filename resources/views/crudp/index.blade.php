@extends('plantilla')
@section('contenido')
<div class="row mt-3">
    <div class="col-12 col-lg-8 offset-8 offset-lg-2 ">
    <div class="d-grid col-lg">
            <button class="btn btn-dark" type="button"  data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <h4>añadir estudiantes</h4>
            </button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Pregrados</th>
                    <th scope="col">Imagen</th>
                    <th scope="col">Editar</th>
                    <th scope="col">Eliminar</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($pregrados as $row)
                <tr>
                    <th scope="row">{{$row->id}}</th>
                    <td>{{$row->nombre}} </td>
                    <td><img style="max-width: 150px;" src="{{$row->img}}" class="img-thumbnail" alt="..."></td>
                    <td><a href="{{url('pregrados',[$row ])}}"><button type="button" class="btn btn-danger"><i class="bi bi-pencil-square"></i></button></a></td>


                    <form action="{{url('pregrados',[$row ])}}" method="post">
                        @csrf
                        @method("delete")

                        <td><button class="btn btn-info"><i class="bi bi-trash3"></i></button></td>
                    </form>


                </tr>
                @endforeach;

            </tbody>
        </table>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">agregar un modal</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data" action="{{url("pregrados")}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Nombre del Pregrado</label>
                        <input name="nombre" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Agrega una imagen</label>
                        <input name="files" type="file" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3 form-check">
                        <label for="example" class="ad">Publicar</label>
                        <input class="form-check-input" type="radio" name="estado" id="activo" value="1" checked>
                    </div>
                    <div class="mb-3 form-check">
                        <label for="example" class="ad">NO Publicar</label>
                        <input class="form-check-input" type="radio" name="estado" id="desactivado" value="0">
                    </div>


                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection