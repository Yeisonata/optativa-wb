@extends('plantilla')
@section('contenido')
<div class="row mt-3">
    <div class="col-12 col-lg-8 offset-8 offset-lg-2 ">
    <div class="d-grid col-lg">
            <button class="btn btn-dark" type="button"  data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <h4>añadir estudiantes</h4>

            </button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Estudiante</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Identificacion</th>
                    <th scope="col">Pregrado</th>
                    <th scope="col">Editar</th>
                    <th scope="col">Eliminar</th>
                </tr>
            </thead>

            <tbody>
                <!-- @foreach ($estudiantes as $row)
                <tr>
                    <th scope="row">{{$row->id}}</th>
                    <td>{{$row->nombre}} </td>
                    <td><img style="max-width: 150px;" src="{{$row->img}}" class="img-thumbnail" alt="..."></td>
                    <td><a href="{{url('pregrados',[$row ])}}"><button type="button" class="btn btn-danger"><i class="bi bi-pencil-square"></i></button></a></td>


                    <form action="{{url('pregrados',[$row ])}}" method="post">
                        @csrf
                        @method("delete")

                        <td><button class="btn btn-info"><i class="bi bi-trash3"></i></button></td>
                    </form>


                </tr>
                @endforeach; -->

            </tbody>
        </table>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">agregar un modal</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data" action="{{url("estudiantes")}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Nombre del Estudiante</label>
                        <input name="nombre" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Correo:</label>
                        <input name="correo" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Identificacion</label>
                        <input name="identificacion" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Seleccione un pregrado</label>
                        <select name="id_pregrados" class="form-select" required>
                        <option value=""></option>
                        @foreach($pregrados as $pregrado)
                            <option value="{{$pregrado->id}}">{{$pregrado->nombre}}</option>
                        
                        @endforeach
                        </select>
                        
                        </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection