@extends('plantilla')
@section('contenido')
<div class="row mt-3">
    <div class="col-12 col-lg-8 offset-8 offset-lg-2 ">
        <div class="d-grid col-lg">
            <button class="btn btn-dark" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <h4>añadir Pregrado</h4>

            </button>

        </div>

        <form method="POST" enctype="multipart/form-data" action="{{url("pregrados",[$pregrados]) }}">
            @method("PUT")
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Nombre del Pregrado</label>
                <input name="nombre" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$pregrados->nombre}}">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Agrega una imagen</label>
                <input name="files" type="file" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3 form-check">
                <label for="example" class="ad">Publicar</label>
                <input class="form-check-input" type="radio" name="estado" id="activo" value="1" {{ $pregrados->estado ==1? 'checked': ''}}>
            </div>
            <div class="mb-3 form-check">
                <label for="example" class="ad">No Publicar</label>
                <input class="form-check-input" type="radio" name="estado" id="desactivado" value="0" value="0" {{ $pregrados->estado ==0? 'checked': ''}}>
            </div>


            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
</div>

<!-- Modal -->


@endsection